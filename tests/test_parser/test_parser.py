from __future__ import unicode_literals

import os

from menudo.parser import Parser
from menudo.option import Option

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
goodConfig = os.path.join(THIS_FOLDER, "goodConfig.yml")


def test_parse():
    """Test parsing a yaml file"""
    parser = Parser(goodConfig)
    config = parser.parse()

    assert config == {
        "name": "Main menu",
        "description": "Main menu 1",
        "pre-start": "./bin/build",
        "options": [
            {
                "name": "Option 1",
                "command": "ls -a",
                "submenu": {
                    "name": "Submenu 1",
                    "description": "Choose an option",
                },
            },
            {"name": "Option 2", "command": "ls"},
        ],
    }


def test_new_menu():
    """Test generating a Menu object from a yaml file"""
    parser = Parser(goodConfig)
    config = parser.parse()
    menu = parser.new_menu(config)

    assert menu.name == "Main menu"
    assert menu.options[1].name == "Option 2"
    assert menu.options[1].submenu == False
