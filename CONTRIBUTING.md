# How to contribute

## Getting Started

We use `poetry` to manage the [dependencies](https://github.com/python-poetry/poetry).
If you dont have `poetry` installed, you should run the command below.

```bash
make download-poetry
```

To install dependencies and prepare [`pre-commit`](https://pre-commit.com/) hooks, run:

```bash
make install
```

If you do not want to install pre-commit hooks, run the command with the NO_PRE_COMMIT flag:

```bash
make install NO_PRE_COMMIT=1
```

To activate your `virtualenv` run:

```
poetry shell
```

> Note: If using VSCode or Pycharm, be sure to run `poetry env list --full-path`, and add correct path
> for your virtual env to your respective settings.

## Linting 

To run the linter, run:

```bash
make lint
```

the same as:

```bash
make test && make check-safety && make check-style
```

> List of flags for `lint` (can be set to `1` or `0`): 
> `STRICT`, `POETRY_STRICT`, `PIP_STRICT`, `SAFETY_STRICT`, `BANDIT_STRICT`, 
> `BLACK_STRICT`, `DARGLINT_STRICT`, `ISORT_STRICT`, `MYPY_STRICT`.

## Running Tests

For information on running tests, check out our development [README.md](dev/README.md).

## Codestyle

After you run `make install` you can use automatic code formatting with:

```bash
make codestyle
```

The command is similar to `check-safety` but to check the code style, obviously. 
It uses `Black`, `Darglint`, `Isort`, and `Mypy` inside.

```bash
make check-style
```

It may also contain the `STRICT` flag.

```bash
make check-style STRICT=1
```

> List of flags for `check-style` (can be set to `1` or `0`): 
> `STRICT`, `BLACK_STRICT`, `DARGLINT_STRICT`, `ISORT_STRICT`, `MYPY_STRICT`.

### Before submitting a Merge Request

Before submitting your code please do the following steps:

1. Add any changes you want
1. Add tests for the new changes
1. Edit documentation if you have changed something significant
1. Run `make codestyle` to format your changes.
1. Run `STRICT=1 make check-style` to ensure that types and docs are correct
1. Run `STRICT=1 make check-safety` to ensure that security of your code is correct

### Releasing

To release a new version of Menudo:

- Create a branch with `git checkout -b bump-version`
- Bump the version of the package `poetry version <version>`. 
  You can pass the new version explicitly, or a rule such as `major`, `minor`, or `patch`. 
  For more details, refer to the [Semantic Versions](https://semver.org/) standard.
- Commit your changes, for example, with `git commit -m "Bump version to x.x.x`
- Create a merge request in Gitlab

#### Publishing

- To publish, run: 

```bash
poetry publish --build
```

### Security 

To run security checks, run:

```bash
make check-safety
```

This command launches a `Poetry` and `Pip` integrity check as well as 
identifies security issues with `Safety` and `Bandit`. By default, 
the build will not crash if any of the items fail. But you can set `STRICT=1` 
for the entire build, or you can configure strictness for each item separately.

```bash
make check-safety STRICT=1
```

or only for `safety`:

```bash
make check-safety SAFETY_STRICT=1
```

multiple

```bash
make check-safety PIP_STRICT=1 SAFETY_STRICT=1
```

> List of flags for `check-safety` (can be set to `1` or `0`): 
> `STRICT`, `POETRY_STRICT`, `PIP_STRICT`, `SAFETY_STRICT`, `BANDIT_STRICT`.


## Credits

The following tools and features are used in development:

- Supports for `Python 3.7` and higher.
- [`Poetry`](https://python-poetry.org/) as the dependencies manager.
  See configuration in [`pyproject.toml`](./pyproject.toml) and [`setup.cfg`](./setup.cfg).
- Power of [`black`](https://github.com/psf/black), [`isort`](https://github.com/timothycrosley/isort)
  and [`pyupgrade`](https://github.com/asottile/pyupgrade) formatters.
- Ready-to-use [`pre-commit`](https://pre-commit.com/) hooks with formatters above.
- Type checks with the configured [`mypy`](https://mypy.readthedocs.io).
- Testing with [`pytest`](https://docs.pytest.org/en/latest/).
- Docstring checks with [`darglint`](https://github.com/terrencepreilly/darglint).
- Security checks with [`safety`](https://github.com/pyupio/safety) and [`bandit`](https://github.com/PyCQA/bandit).
- A thorough [`.editorconfig`](.editorconfig), [`.dockerignore`](.dockerignore), and [`.gitignore`](.gitignore).

To help with community contributions, we have:

- Ready-to-use [Pull Requests templates](./.gitlab/merge_request_templates) and 
  several [Issue templates](./.gitlab/issue_templates).
- [Semantic Versions](https://semver.org/) 
