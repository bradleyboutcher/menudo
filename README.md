# menudo

[![pipeline status](https://gitlab.com/bradleyboutcher/menudo/badges/main/pipeline.svg)](https://gitlab.com/bradleyboutcher/menudo/-/commits/main)
[![Python Version](https://img.shields.io/pypi/pyversions/menudo.svg)](https://pypi.org/project/menudo/)
[![Dependencies Status](https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg)](https://github.com/menudo/menudo/pulls?utf8=%E2%9C%93&q=is%3Apr%20author%3Aapp%2Fdependabot)

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Security: bandit](https://img.shields.io/badge/security-bandit-green.svg)](https://github.com/PyCQA/bandit)
[![Pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://gitlab.com/bradleyboutcher/menudo/-/blob/main/.pre-commit-config.yaml)
[![Semantic Versions](https://img.shields.io/badge/%F0%9F%9A%80-semantic%20versions-informational.svg)](https://gitlab.com/bradleyboutcher/menudo/-/releases)
[![License](https://img.shields.io/github/license/menudo/menudo)](https://gitlab.com/bradleyboutcher/menudo/-/blob/main/LICENSE)

Menudo is a Python-based utility for generating main-menus from YAML files, 
to facilitate easier testing and development in your open-source projects

## Installation

> Note: This project is in a pre-Alpha stage, so these installations won't work. 
> Check back later for more information.

```bash
pip install -U menudo
```

or install with `Poetry`

```bash
poetry add menudo
```

Then you can run

```bash
menudo --help
```

## 📈 Releases

You can see the list of available releases on the 
[GitHub Releases](https://gitlab.com/bradleyboutcher/menudo/-/releases) page.

We follow [Semantic Versions](https://semver.org/) specification.

## 🛡 License

[![License](https://img.shields.io/github/license/menudo/menudo)](https://github.com/menudo/menudo/blob/master/LICENSE)

This project is licensed under the terms of the `GNU GPL v3.0` license. See [LICENSE](https://github.com/menudo/menudo/blob/master/LICENSE) for more details.

## 📃 Citation

```
@misc{menudo,
  author = {menudo},
  title = {Menudo is a Python-based utility for generating main-menus from YAML files, to facilitate easier testing and development in your open-source projects},
  year = {2021},
  publisher = {GitHub},
  journal = {GitHub repository},
  howpublished = {\url{https://github.com/menudo/menudo}}
}
```

## Credits

This project was originally generated with [`python-package-template`](https://github.com/TezRomacH/python-package-template).
It was a huge help getting started, and includes detailed explanations for each utility. 

If you're making a new Python project, give it a try!
