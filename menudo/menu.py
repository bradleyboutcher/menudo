class Menu:
    def __init__(
        self, name: str, description: str, pre_start: str, options: bytearray
    ):
        super().__init__()
        self.name = name
        self.description = description
        self.pre_start = pre_start
        self.options = options

    def start(self):
        if self.pre_start != "" and self.pre_start is not None:
            print(self.pre_start)
        print(self.name)
        print(self.description)
        print(self.options)
