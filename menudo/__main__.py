# type: ignore[attr-defined]

import typer
from rich.console import Console

app = typer.Typer(
    name="menudo",
    help="Menudo is a Python-based utility for generating main-menus from YAML files,"
    + "to facilitate easier testing and development in your open-source projects",
    add_completion=False,
)
console = Console()


def main(config_file: str):
    typer.echo(config_file)


if __name__ == "__main__":
    typer.run(main)
