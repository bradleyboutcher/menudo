import yaml

from .menu import Menu
from .option import Option


class Parser:
    def __init__(self, config_file):
        self.config_file = config_file

    def parse(self):
        with open(self.config_file) as f:
            # use safe_load instead load
            return yaml.safe_load(f)

    def new_menu(self, config):
        options = [
            Option(
                o.get("name", "Option"),
                o.get("command", "no command"),
                o.get("submenu", False),
            )
            for o in config["options"]
        ]

        return Menu(
            config["name"],
            config["description"],
            config["pre-start"],
            options,
        )


new_menu = Parser.new_menu
