class Option:
    def __init__(self, name: str, command: str, submenu: bool):
        super().__init__()
        self.name = name
        self.command = command
        self.submenu = submenu
