# type: ignore[attr-defined]
"""Menudo is a Python-based utility for generating main-menus from YAML files, 
to facilitate easier testing and development in your open-source projects"""

try:
    from importlib.metadata import PackageNotFoundError, version
except ImportError:  # pragma: no cover
    from importlib_metadata import PackageNotFoundError, version


try:
    __version__ = version(__name__)
except PackageNotFoundError:  # pragma: no cover
    __version__ = "unknown"
