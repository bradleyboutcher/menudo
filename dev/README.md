This document contains 

## Running tests

To run tests locally, use:
```bash
./dev/run_unit
```

This will handle building the necessary image with all dependencies 
installed, and run `poetry run pytest` inside the container.

## Using the Makefile
To create the Docker using the Makefile, you can run:

```bash
make docker
```

which is equivalent to:

```bash
make docker VERSION=latest
```

You could also provide name and version for the image itself.
Default name is `IMAGE := menudo`.
Default version is `VERSION := latest`.

```bash
make docker IMAGE=some_name VERSION=0.1.0
```

### Running tests manually

```bash
docker run -it --rm \
   -v $(pwd):/workspace \
   menudo bash
```

### Cleaning Up

To uninstall docker image run `make clean_docker` with `VERSION`:

```bash
make clean_docker VERSION=0.1.0
```

like in installation, you can also choose the image name

```bash
make clean_docker IMAGE=some_name VERSION=latest
```

If you want to clean everything up, including `build`, run:

```bash
make clean
```
